# DOI to SciHub

Android app for quickly opening Sci-Hub links from the Digital Object Identifier's URL (using Android intents).

Note: This app is solely intended for assistance in accessing freely available research articles, not for circumventing paywalls or breaking the law.

(Built with Flutter)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.sigmarelax.doitoscihub/)
